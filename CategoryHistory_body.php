<?php

/**
 * This page displays recent changes of the category, which name is displayed at the end of the URL.
 * The link for the history is available at every Category page (under the title)
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class CategoryHistory extends SpecialPage {

	function __construct() {
		parent::__construct( 'CategoryHistory' );

	}
 
	function execute($category) {
		
		// read config variables
		$config = $this->getConfig();
		$cathist_page_limit = $config->get( 'cathist_page_limit' );
		$cathist_name_style = $config->get( 'cathist_name_style' );
		
		$this->setHeaders();
		$request = $this->getRequest();
		$out = $this->getOutput();

 		if(!$category) {
			$out->mBodytext .= $this->msg('categoryhistory-desc')->parse();
		}
 		else {
 			$category = addslashes(strip_tags( $category ));
			$ch_type = $request->getVal('ch_type');
			if(empty($ch_type)) {
				$ch_type = "showtable";
			}
			
			// display page title
			$dispname = stripslashes($category);
			$dispname = preg_replace("/_/", " ", $dispname);
			$out->setPagetitle($this->msg('categoryhistory')->text()." - " . $dispname );

			$page = $request->getInt('catHistoryPageNumber',1);

			// parse all revisions of categories' articles
			$conn = \MediaWiki\MediaWikiServices::getInstance()->getDBLoadBalancer();
			$dbr = $conn->getConnectionRef(DB_REPLICA);
			$res = $dbr->select(
				"categorylinks",
				array("cl_from"),
				"cl_to = '$category'"
			);

			if($res->numRows()>0) { 
				// category has some articles
				
				// preparing conditions and options for the final SELECT
				$conds = "";
				foreach($res as $row) {
 					if($conds) {
						$conds .= " OR ";
					}
					$conds .= "revision.rev_page = " . $row->cl_from;
				}
				$options = " ORDER BY revision.rev_timestamp DESC LIMIT " . $cathist_page_limit . " OFFSET " . (($page-1) * $cathist_page_limit);

				/** display pager **/
				$output = "<style>";
				$output .= "input {background-color:white;border-width:0px;}";
				$output .= "</style>";
				$output .= "<form id='catHistMenu' name='catHistMenu' method='post' action=''>\n";
				$output .= "<nav aria-label='pager'>\n<ul class='pagination'>\n";

				// previous
				if($page==1) {
					$prev = 1;
				}
				else {
					$prev = $page-1;
				}
				if($page > 1) {
					$output .= "<li class='page-link'><a href='#' aria-label='Previous'><span aria-hidden='true' onclick=";
					$output .= "\"document.getElementById('catHistoryPageNumber').value=$prev;";
					$output .= "document.getElementById('catHistMenu').submit();\" title='";
					$output .= $this->msg('mwe-cathist-previous')->text();
					$output .= "'>&laquo;</span></a></li>\n";
				}
				else {
					$output .= "<li class='page-link disabled'><a href='#' aria-label='Previous' aria-disabled='true'><span aria-hidden='true' title='";
					$output .= $this->msg('mwe-cathist-previous')->text();
					$output .= "'>&laquo;</span></a><li>";
				}

				// show the number of pages
				$res = $dbr->select(
  					"revision",
					array("rev_page"),
					$conds
				);
				$nm = 1 + floor($res->numRows()/$cathist_page_limit);
				for($i=1;$i<=$nm;$i++){
					if($i==$page) {
						$output .= "<li class='page-link active bg-primary'><a href='#' class='text-white' aria-label='Page $i' aria-current='page'>$i</a></li>\n";
					}
					else {
						$output .= "<li class='page-link'><a href='#' aria-label='Page $i' ";
						$output .= "onclick=\"document.getElementById('catHistoryPageNumber')";
						$output .= ".value=$i;document.getElementById('catHistMenu').submit();\">$i</a></li>\n";
					}
				}
				// next
				if($page<$nm) {
					$next = $page+1; 
				}
				else {
					$next = $nm;
				}
				if($page < $nm) {
					$output .= "<li class='page-link'><a href='#' aria-label='Next'><span aria-hidden='true' onclick=";
					$output .= "\"document.getElementById('catHistoryPageNumber').value=$next;";
					$output .= "document.getElementById('catHistMenu').submit();\" title='";
					$output .= $this->msg('mwe-cathist-next')->text();
					$output .= "'>&raquo;</span></a>\n";
				}
				else {
					$output .= "<li class='page-link disabled'><a href='#' aria-label='Next' aria-disabled='true'><span aria-hidden='true' title='";
					$output .= $this->msg('mwe-cathist-next')->text();
					$output .= "'>&raquo;</span></a>\n";
				}
				$output .= "<input id='catHistoryPageNumber' name='catHistoryPageNumber' ";
				$output .= "type='hidden' value='1'/>\n";
				$output .= "<input id='ch_type' name='ch_type' type='hidden' value='$ch_type'/>\n";
				
				$output .= "</ul>\n</nav>\n";
				$output .= "</form>\n";
				if ($ch_type != 'stats') $out->mBodytext .= $output;
				
				/** pager END */

				// switch table/wikicode/stats
				$wc_disabled = $st_disabled = $stat_disabled = '';
				if($ch_type == 'wikicode') $wc_disabled = 'disabled';
				elseif($ch_type == 'showtable') $st_disabled = 'disabled';
				elseif($ch_type == 'stats') $stat_disabled = 'disabled';

				$out->mBodytext .= "<div class='mb-6 mt-0'>\n";
				$out->mBodytext .= "<a href='./$category?catHistoryPageNumber=$page&ch_type=showtable' class='btn btn-primary btn-sm text-white me-2 $st_disabled'>" . $this->msg('mwe-cathist-showtable')->text() . "</a>\n";
				$out->mBodytext .= "<a href='./$category?catHistoryPageNumber=$page&ch_type=wikicode' class='btn btn-primary btn-sm text-white me-2 $wc_disabled'>" . $this->msg('mwe-cathist-showwiki')->text() . "</a>\n";
				$out->mBodytext .= "<a href='./$category?catHistoryPageNumber=$page&ch_type=stats' class='btn btn-primary btn-sm text-white $stat_disabled'>" . $this->msg('mwe-cathist-showstat')->text() . "</a>\n";
				$out->mBodytext .= "</div>\n";

				$sql = "SELECT revision.rev_id,revision.rev_page, revision.rev_timestamp, revision.rev_minor_edit,revision.rev_deleted, page_title, page_namespace, ";
				$sql .= "(SELECT comment.comment_text FROM comment LEFT JOIN revision_comment_temp ON ( comment.comment_id = revision_comment_temp.revcomment_comment_id) ";
				$sql .= "WHERE revision_comment_temp.revcomment_rev = revision.rev_id) AS comment,";
				//$sql .= "(SELECT CONCAT(user.user_id, '||', user.user_name, '||', user.user_real_name) FROM user LEFT JOIN actor ON (user.user_id = actor.actor_user) LEFT JOIN revision_actor_temp ON (revision_actor_temp.revactor_actor = actor.actor_id) ";
				//$sql .= "WHERE revision_actor_temp.revactor_rev = revision.rev_id) AS userinfo ";
				$sql .= "(SELECT CONCAT(user.user_id, '||', user.user_name, '||', user.user_real_name) FROM user LEFT JOIN actor ON (user.user_id = actor.actor_user) ";
				$sql .= "WHERE actor.actor_id = revision.rev_actor) AS userinfo ";
				$sql .= "FROM revision ";
				$sql .= "INNER JOIN page ON (page.page_id = revision.rev_page) ";
				$sql .= "WHERE ($conds)";

				if ($ch_type == 'stats') {
					// category statistics
					$stats_start = $request->getVal('stats_start', '');
					$stats_end = $request->getVal('stats_end', '');
					$stats_start_total = $request->getVal('stats_start_total', '');
					$stats_end_total = $request->getVal('stats_end_total', '');
					$total_users = 0;
					$total_edits = 0;
					$total_pages = 0;
					$total_first_ts = '';
					$total_last_ts = '';
					if($stats_start) $sql .= " AND revision.rev_timestamp > " . preg_replace('/-/', '', $stats_start) . "000000 ";
					if($stats_end) $sql .= " AND revision.rev_timestamp <= " . preg_replace('/-/', '', $stats_end) . "235959 ";
					$sql .= " ORDER BY revision.rev_timestamp";

					$res = $dbr->query($sql);
					$pages = [];
					$users = [];
					foreach($res as $row) {
						preg_match('/\|\|(.*)\|\|.*/', $row->userinfo, $m);
						if(!isset($m[1])) $user_name = 'Anonymous'; else $user_name = $m[1];
						$name = str_replace("_"," ",$row->page_title);
						if(!empty($row->page_namespace)) {
							$name = "{{ns:".$row->page_namespace."}}:$name";
						}
						if(!array_key_exists($name, $pages)) {
							$pages[$name] = [
								'first_ts' => $row->rev_timestamp,
								'last_ts' => $row->rev_timestamp,
								'rev_count' => 1,
								'users' => [$user_name]
							];
							$total_pages++;
							$total_edits++;
						}
						else {
							$pages[$name]['last_ts'] = $row->rev_timestamp;
							$pages[$name]['rev_count'] ++;
							$total_edits++;
							if(!in_array($user_name, $pages[$name]['users'])) array_push($pages[$name]['users'], $user_name);
						}
						if(!$total_first_ts) $total_first_ts = $row->rev_timestamp;
						if($row->rev_timestamp > $total_last_ts) $total_last_ts = $row->rev_timestamp;

						if(!array_key_exists($user_name, $users)) {
							$users[$user_name] = [
								'first_ts' => $row->rev_timestamp,
								'last_ts' => $row->rev_timestamp,
								'rev_count' => 1,
								'pages' => [$name]
							];
							$total_users++;
						}
						else {
							$users[$user_name]['last_ts'] = $row->rev_timestamp;
							$users[$user_name]['rev_count'] ++;
							$pgs = $users[$user_name]['pages'];
							if(!in_array($name, $users[$user_name]['pages'])) array_push($users[$user_name]['pages'], $name);
						}
					}

					// add time interval selector
					if($stats_start_total) $start = $stats_start_total;
					else $start = $stats_start_total = $total_first_ts;
					if($stats_end_total) $stop = $stats_end_total;
					else $stop = $stats_end_total = $total_last_ts;
					$dt = new DateTime(preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', '$1-$2-$3 00:00:00', $start));
					$stopDate = new DateTime(preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', '$1-$2-$3 00:00:00', $stop));
					$startOptions = '';
					$endOptions = '';
					while($dt <= $stopDate) {
						$opt = $dt->format('Y-m-d');
						$startOptions .= "<option value='$opt' ";
						if($opt == $stats_start) $startOptions .= "selected";
						$startOptions .= ">$opt</option>\n";
						$endOptions .= "<option value='$opt' ";
						if($opt == $stats_end) $endOptions .= "selected";
						$endOptions .= ">$opt</option>\n";
						$dt->add(new DateInterval('P1D'));
					}
					$intSel = "<div class='mt-4 mb-4'>\n<form class='form-inline' method='post'>\n";
					$intSel .= "<input id='ch_type' name='ch_type' type='hidden' value='stats'/>\n";
					$intSel .= "<input name='stats_start_total' type='hidden' value='$stats_start_total'/>\n";
					$intSel .= "<input name='stats_end_total' type='hidden' value='$stats_end_total'/>\n";
					$intSel .= "<div class='form-group me-3'>\n";
					$intSel .= "<label for='stats_start' class='me-3'>" . $this->msg('mwe-cathist-interval-from')->text() . "</label>\n";
					$intSel .= "<select class='form-control' id='stats_start' name='stats_start'>\n";
					$intSel .= "<option value=''>-- default --</option>\n";
					$intSel .= $startOptions;
					$intSel .= "</select>\n";
					$intSel .= "</div>\n";
					$intSel .= "<div class='form-group me-4'>\n";
					$intSel .= "<label for='stats_end' class='me-3'>" . $this->msg('mwe-cathist-interval-to')->text() . "</label>\n";
					$intSel .= "<select class='form-control' id='stats_end' name='stats_end'>\n";
					$intSel .= "<option value=''>-- default --</option>\n";
					$intSel .= $endOptions;
					$intSel .= "</select>\n";
					$intSel .= "</div>\n";
					$intSel .= "<input class='btn btn-primary' type='submit' value='" . $this->msg('mwe-cathist-submit')->text() . "'>\n";
					$intSel .= "</form>\n</div>\n";
					$out->mBodytext .= $intSel;

					// sum
					$sum = "{| class = 'wikitable' \n";
					$sum .= "|- \n";
					$sum .= "! style='text-align: left;'|" . $this->msg('mwe-cathist-pagescounttotals')->text() . "\n";
					$sum .= "| " . $total_pages . "\n";
					$sum .= "|- \n";
					$sum .= "! style='text-align: left;'|" . $this->msg('mwe-cathist-revcounttotals')->text() . "\n";
					$sum .= "| " . $total_edits . "\n";
					$sum .= "|- \n";
					$sum .= "! style='text-align: left;'|" . $this->msg('mwe-cathist-usercounttotals')->text() . "\n";
					$sum .= "| " . $total_users . "\n";
					$sum .= "|} ";
					$out->addWikiTextAsInterface($sum);
					$out->mBodytext .= "<div class='mb-5'>" . $this->msg('mwe-cathist-anon-info')->text() . "</div>";

					/** page view **/
					ksort($pages);
					$table = "{| class = 'wikitable sortable'\n";
					$table .= "|- \n";
					$table .= "! width='300'|" . $this->msg('mwe-cathist-pagename')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-firstrev-ts')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-lastrev-ts')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-revcount')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-usercount')->text() . "\n";
					foreach($pages as $key=>$value) {
						$table .= "|- \n";
						$name="[{{fullurl:$key|action=history}} $key]";
						$table .= "| $name\n";
						$table .= "| " . preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', "$1-$2-$3", $value['first_ts']) . "\n";
						$table .= "| " . preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', "$1-$2-$3", $value['last_ts']) . "\n";
						$table .= "| " . $value['rev_count'] . "\n";
						$table .= "| " . sizeof($value['users']) . "\n";
					}
					$table .= "|} \n\n";
					$out->addWikiTextAsInterface($table);

					/** users view **/
					ksort($users);
					$table = "{| class = 'wikitable sortable'\n";
					$table .= "|- \n";
					$table .= "! width='300'|" . $this->msg('mwe-cathist-user')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-firstrev-ts')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-lastrev-ts')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-revcount')->text() . "\n";
					$table .= "! width='110'|" . $this->msg('mwe-cathist-pagescount')->text() . "\n";
					foreach($users as $key=>$value) {
						$table .= "|- \n";
						if($key == 'Anonymous') $table .= "| '''Anonymous'''\n";
						else $table .= "| [[User:$key|$key]]\n";
						$table .= "| " . preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', "$1-$2-$3", $value['first_ts']) . "\n";
						$table .= "| " . preg_replace('/([0-9]{4})([0-9]{2})([0-9]{2}).*/', "$1-$2-$3", $value['last_ts']) . "\n";
						$table .= "| " . $value['rev_count'] . "\n";
						$table .= "| " . sizeof($value['pages']) . "\n";
					}
					$table .= "|} \n\n\n\n";
					$out->addWikiTextAsInterface($table);
				}
				else {
					// paged category changes
					$sql .= " $options";
					$res = $dbr->query($sql);
					
					// table header
					$table = "{| class = 'wikitable'\n";
					$table .= "|- \n";
					$table .= "! width='110'|".$this->msg('mwe-cathist-time')->text()."\n";
					$table .= "! width='300'|".$this->msg('mwe-cathist-pagename')->text()."\n";
					$table .= "! width='230'|".$this->msg('mwe-cathist-author')->text()."\n";
					$table .= "! ".$this->msg('mwe-cathist-desc')->text()."\n";
					$table .= "! width='40'|".$this->msg('mwe-cathist-flags')->text()."\n";
					if($ch_type=="wikicode"){
						$table .= "! ".$this->msg('mwe-cathist-notice')->text()." 1\n";
						$table .= "! ".$this->msg('mwe-cathist-notice')->text()." 2\n";
					}
					
					// display rows with changes
					foreach($res as $row) {
						$table .= "|- \n";
						// datum
						$table .= "| ".substr($row->rev_timestamp,0,4)."-";
						$table .= substr($row->rev_timestamp,4,2)."-";
						$table .= substr($row->rev_timestamp,6,2)." ";
						$table .= substr($row->rev_timestamp,8,2).":";
						$table .= substr($row->rev_timestamp,10,2)."\n";
						
						$name = str_replace("_"," ",$row->page_title);
						if(!empty($row->page_namespace)) {
							$name = "{{ns:".$row->page_namespace."}}:$name";
						}
						$name="[{{fullurl:$name|action=history}} $name]";
						$table .= "| $name\n";
		
						$arr = explode('||', $row->userinfo);
						$user_id = $arr[0];
						if(sizeof($arr) > 1) $user_name = $arr[1]; else $user_name = '';
						if(sizeof($arr) > 2) $user_real_name = $arr[2]; else $user_real_name = '';

						if($cathist_name_style != "REALNAME") {
							if(!empty($user_name)) {
								$user = "[[User:".$user_name."|".$user_name."]]";
							}
							else {
								$user = $user_name;
							}
							if($cathist_name_style == "BOTH" && !empty($user_real_name)) {
								$user .= " (".$user_real_name.")";
							}
						}
						else{
							if(!empty($user_real_name)) {
								$user = "[[User:". $user_name."|". $user_real_name."]]";
							}
							elseif(!empty($user_name)) {
								$user = "[[User:".$user_name."|".$user_name."]]";
							}
							else {
								$user = $user_name;
							}
						}				
						$table .= "| $user\n";
						
						// comment
						if(!empty($row->comment)) {
							$comment = $row->comment;
						}
						else {
							$comment = "";
						}
						$comment = "<nowiki>$comment</nowiki>";
						$table .= "| $comment\n";
						
						// article deleted?
						if(!empty($row->rev_deleted)) {
							$deleted = "d"; 
						}
						else {
							$deleted = "";
						}
						
						// minor edit
						if(!empty($row->rev_minor_edit)) {
							$me = "me"; 
						}
						else {
							$me = "";
						}
						$table .= "| $me";
						if($deleted) {
							$table .= "-$deleted";
						}
						$table .= "\n";
						if($ch_type=="wikicode"){
							$table .= "| \n| \n";
						}
					}
					$table .= "|} \n\n";
					
					if($ch_type=="wikicode"){
						// display wikicode of the table
						$out->mBodytext .= nl2br($table);
						$out->mBodytext .= $this->msg('mwe-cathist-legend')->parse()."<br/>";
						$out->mBodytext .= $this->msg('mwe-cathist-legend-flags')->parse()."<br/>";
					}
					else{
						// display table
						$out->addWikiTextAsInterface($table);
                        $out->mBodytext .= $this->msg('mwe-cathist-legend')->parse()."<br/>";
						$out->mBodytext .= $this->msg('mwe-cathist-legend-flags')->parse()."<br/>";
					}
				}
			}
		}
		
 	}
}

?>
