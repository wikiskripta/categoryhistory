# CategoryHistory

Mediawiki extension.

## Description

* Extension shows all changes in category - added/removed/changed articles.
* Link to a special page can be found bellow the category header.
* Table with changes can be copied in WIKI format.
* Version 2.1

## Instalation

* Make sure you have MediaWiki 1.39+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'CategoryHistory' )`;
* Set variables in config section of _extension.json_.

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Release Notes

### 1.2

* Database structure has changed. SQL selects rewritten for MW 1.36.

### 2.0

* New category statistics

### 2.1

* Database structure has changed. SQL selects rewritten for MW 1.39.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart), [Petr Kajzar](https://www.wikiskripta.eu/w/User:Slepi)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University