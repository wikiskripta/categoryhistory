<?php

/**
 * All hooked functions used by CategoryHistory
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class CategoryHistoryHooks {

	/**
	 * Set up the parser hooks
	 * @param object $skin: instance of Skin object
	 * @param string &$subpages: subpage links HTML
	 * @return Boolean: true
	 */
	public static function AddLinkToCategory(&$subpages,$skin) {
	
		$title = $skin->getRelevantTitle();
		if($title->getNamespace() == 14) {
			// category page
			$url = str_replace("/".$title->getSubjectNsText().":","/Special:CategoryHistory/", $title->getFullURL());
			$subpages .= Html::element('a',array('href' => $url),$skin->msg('mwe-cathist-link')->text());
		}
		return true;
	}

	
}